import paramiko

ssh_client = paramiko.SSHClient()
ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh_client.connect(hostname="10.101.12.202", port=22, username="root", password="iamroot")

stdin,stdout,stderr = ssh_client.exec_command("nmap -v -A www.ateneo.edu ")
print(stdout)
for x in stdout.readlines():
    print(x)

stdin,stdout,stderr = ssh_client.exec_command("nmap -O www.ateneo.edu ")
print(stdout)
for x in stdout.readlines():
    print(x)

stdin,stdout,stderr = ssh_client.exec_command("nmap -v -A localhost")
print(stdout)
for x in stdout.readlines():
    print(x)
ssh_client.close()